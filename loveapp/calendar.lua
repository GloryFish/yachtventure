require 'middleclass'
require 'notifier'

Calendar = class('Calendar')

local days = {
  'Sun',
  'Mon',
  'Tue',
  'Wed',
  'Thu',
  'Fri',
  'Sat',
}

function Calendar:initialize()
  self.currentDay = 1

  self.week = {}

  Notifier:listenForMessage('next_day', self)
end

function Calendar:getWeek()
  local week = {}
  for day = self.currentDay, self.currentDay + 6 do
    table.insert(week, days[(day % 7) + 1])
  end
  return week
end

function Calendar:getToday()
  local week = self:getWeek()
  return week[1]
end

function Calendar:isWeekend()
local today = self:getToday()
  return today == 'Fri' or today == 'Sat'
end

function Calendar:receiveMessage(message, data)
  if message == 'next_day' then
    self.currentDay = self.currentDay + 1
    Notifier:postMessage('calendar_changed', self:getWeek())
  end
end