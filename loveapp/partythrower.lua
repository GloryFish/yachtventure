require 'middleclass'
require 'utility'

PartyThrower = class('PartyThrower')

function PartyThrower:initialize(player, ship, weather, calendar)
  self.player = player
  self.ship = ship
  self.weather = weather
  self.calendar = calendar
end

-- Calculates details for a party and returns them as a table of values.
function PartyThrower:throwPartyAtPort(port)
  local totalScore = 0
  local details = {
    port = port,
    scoring = {},
    bonuses = {},
  }
  local weather = self.weather:getToday()

  -- Weather
  if weather == 'Sunny' then
    totalScore = totalScore + 10
    table.insert(details.scoring, { description = 'It was a Sunny day!', score = 10 })

  elseif weather == 'Cloudy' then
    totalScore = totalScore - 10
    table.insert(details.scoring, { description = 'It was a Cloudy day.', score = -10 })
  else
    table.insert(details.scoring, { description = 'It was a Fair day.', score = 0 })
  end

  -- Supplies
  if self.ship:hasSupplies() then
    totalScore = totalScore + 5
    table.insert(details.scoring, { description = 'The ship was well-stocked.', score = 10 })
  else
    totalScore = totalScore - 5
    table.insert(details.scoring, { description = 'You were all out of cups.', score = -10 })
  end

  -- Boat score
  totalScore = totalScore + self.ship:getScore()
  table.insert(details.scoring, { description = 'Your Yacht was worth some points.', score = self.ship:getScore() })

  -- Attendance
  local baseAttendance = math.random(port.attendeeMin, port.attendeeMax)
  local modifiedAttendance = baseAttendance + (math.clamp(self.player:getReputation() - port.reputationStandard, 0, 4) * 0.25 * (port.attendeeMax - port.attendeeMin))
  local attendance = math.clamp(modifiedAttendance, port.attendeeMin, port.attendeeMax)

  totalScore = totalScore + attendance
  table.insert(details.scoring, { description = string.format('%d people came!', attendance), score = attendance })


  -- 6. A top-notch crew
  -- TODO: Crew system

  -- 7. A weekend!
  if self.calendar:isWeekend() then
    totalScore = totalScore + 5
    table.insert(details.scoring, { description = 'Weekend parties are the best.', score = 5 })
  end

  details.totalScore = totalScore

  return details
end