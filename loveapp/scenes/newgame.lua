--
--  base.lua
--  rogue-descent
--
--  A base scene that can be used when creating new scenes.
--
--  Created by Jay Roberts on 2012-02-23.
--  Copyright 2012 Jay Roberts. All rights reserved.
--

require 'logger'
require 'vector'
require 'colors'
require 'rectangle'
require 'label'
require 'player'

local scene = Gamestate.new()

function scene:enter(pre)
  self.nameLabel = Label(fonts.default)
  self.nameLabel.text = 'Applicant Name'
  self.nameLabel.anchor = vector(0, 0)
  self.nameLabel.position = vector(20, 70)

  self.nameTextfield = loveframes.Create('textinput')
  self.nameTextfield:SetPos(20, 85)
  self.nameTextfield:SetFont(fonts.default)
  self.nameTextfield:SetSize(100, 20)
  self.nameTextfield:SetFocus(true)
  self.nameTextfield.OnEnter = function()
    self:startGame()
  end

  local gobutton = loveframes.Create('imagebutton', frame)
  gobutton:SetPos(20, 120)
  gobutton:SetSize(34, 16)
  gobutton:SetText('Go!')
  gobutton:SetImage('resources/sprites/button_ok.png')
  gobutton.OnClick = function()
    self:startGame()
  end
end

function scene:textinput(text)
  loveframes.textinput(text)
end

function scene:keypressed(key, isrepeat)
  loveframes.keypressed(key, isrepeat)

  if key == 'escape' then
    self:quit()
  end
end

function scene:keyreleased(key, isrepeat)
  loveframes.keyreleased(key, isrepeat)
end

function scene:mousepressed(x, y, button)
  loveframes.mousepressed(x, y, button)
end

function scene:mousereleased(x, y, button)
  loveframes.mousereleased(x, y, button)
end

function scene:update(dt)
  Timer.update(dt)
  Cursor:update(dt)

  loveframes.update(dt)

end

function scene:draw()
  love.graphics.clear()
  colors.white:set()

  love.graphics.setCanvas(Canvases.buffer)
  love.graphics.clear(colors.grey:unpack())
  love.graphics.setShader()

  self.nameLabel:draw()

  loveframes.draw()

  Sprites.ui.batch:clear()
  Cursor:draw()
  love.graphics.draw(Sprites.ui.batch)

  love.graphics.setCanvas()
  Shaders:set(Shaders.currentEffect)

  love.graphics.draw(Canvases.buffer, Shaders.quad, 0, 0)
  love.graphics.setShader()

  Console:draw()
end

function scene:startGame()
  local player = Player()
  player:setName(self.nameTextfield:GetText())
  scenes.game.player = player
  loveframes.util.RemoveAll()
  Gamestate.switch(scenes.game)
end

function scene:quit()
  Gamestate.switch(scenes.title)
end

function scene:leave()
end

return scene