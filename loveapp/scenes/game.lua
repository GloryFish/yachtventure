--
--  game.lua
--

require 'logger'
require 'vector'
require 'colors'
require 'rectangle'
require 'agent'
require 'entity'
require 'camera'
require 'background'
require 'station'
require 'gesturerecognizer'
require 'world'
require 'ship'
require 'weather'
require 'calendar'
require 'partythrower'
require 'ui/portinfoframe'
require 'ui/repinfoframe'
require 'ui/shipinfoframe'
require 'ui/crewinfoframe'
require 'ui/financeinfoframe'
require 'ui/partyframe'
require 'ui/hud'
require 'ui/pausemenu'
require 'ui/messagecenter'

local scene = Gamestate.new()

function scene:enter(pre)
  Console.delegate = self

  self.gesturerecognizer = GestureRecognizer()

  love.mouse.setGrabbed(true)

  self.logger = Logger()

  self.drawables = {}
  self.updatables = {}

  self.camera = Camera()
  self.camera.smoothMovement = false
  self.camera.focus = vector(100, 100)
  self.camera.handheld = false
  self.camera.scale = 1
  self.camera.useBounds = true

  self.paused = false
  self.timescale = 1

  self.world = World('resources/maps/worldmap.tmx')
  self.world.camera = self.camera
  table.insert(self.updatables, self.world)

  self.camera.bounds = self.world:getBounds()

  self.weather = Weather()
  self.calendar = Calendar()

  Notifier:listenForMessage('mouse_moved', self)
  Notifier:listenForMessage('mouse_down', self)
  Notifier:listenForMessage('mouse_down_right', self)
  Notifier:listenForMessage('mouse_click', self)
  Notifier:listenForMessage('world_down', self)
  Notifier:listenForMessage('port_selected', self)
  Notifier:listenForMessage('show_infoframe', self)
  Notifier:listenForMessage('next_day', self)
  Notifier:listenForMessage('throw_party', self)

  -- Temp, add a playership for testing
  self.ship = Ship('envynautilus')
  self.ship.world = self.world
  self.ship:setPosition(self.world:getPlayerSpawn())
  table.insert(self.updatables, self.ship)

  self.partyThrower = PartyThrower(self.player, self.ship, self.weather, self.calendar)

  -- UI components
  self.portInfoFrame = PortInfoFrame(self.world, self.player, self.ship, self.weather)
  self.repInfoFrame = RepInfoFrame(self.player)
  self.shipInfoFrame = ShipInfoFrame(self.ship)
  self.crewInfoFrame = CrewInfoFrame()
  self.financeInfoFrame = FinanceInfoFrame()
  self.partyFrame = PartyFrame()

  self.hud = Hud(self.ship, self.player, self.weather, self.calendar, self)
  self.hud:show()
  self.pauseMenu = PauseMenu(self)

  self.messagecenter = MessageCenter()

  print('Starting game for player: ' .. self.player:getName())

end

function scene:textinput(text)
end

function scene:keypressed(key, isrepeat)
  if Console:keypressed(key, isrepeat) then
    return
  end

  loveframes.keypressed(key, isrepeat)

  if key == 'escape' then
    if self.portInfoFrame:isOpen() then
      self.portInfoFrame:close()
    elseif self.repInfoFrame:isOpen() then
      self.repInfoFrame:close()
    elseif self.shipInfoFrame:isOpen() then
      self.shipInfoFrame:close()
    elseif self.crewInfoFrame:isOpen() then
      self.crewInfoFrame:close()
    elseif self.financeInfoFrame:isOpen() then
      self.financeInfoFrame:close()
    elseif self.partyFrame:isOpen() then
      self.partyFrame:close()
    else
      self:setPaused(not self.paused)
    end
  end

  if key == 'p' then
    self.paused = not self.paused
  end

  if key == ']' then
    self.timescale = self.timescale + 0.5
  end

  if key == '[' then
    self.timescale = self.timescale - 0.5
  end

  if key == '`' then
    Console:open()
  end

  if key == 'f' then
    System:toggleFullscreen()
  end

end

function scene:keyreleased(key, isrepeat)
  loveframes.keyreleased(key, isrepeat)
end

function scene:mousepressed(x, y, button)
  loveframes.mousepressed(x, y, button)
end

function scene:mousereleased(x, y, button)
  loveframes.mousereleased(x, y, button)
end

function scene:receiveMessage(message, data)
  if message == 'mouse_down' then
    local position = data
    local worldPoint = self.camera:screenToWorld(position)

    -- Pass this on to other interested game objects
    Notifier:postMessage('world_down', worldPoint)

  elseif message == 'mouse_moved' then
    local position = data
    local worldPoint = self.camera:screenToWorld(position)

    -- Pass this on to other interested game objects
    Notifier:postMessage('world_moved', worldPoint)

  elseif message == 'world_down' then
    local worldPoint = data

  elseif message == 'port_selected' then
    local port = data
    self.portInfoFrame:showForPort(port)

  elseif message == 'show_infoframe' then
    local frame = data
    if frame == 'reputation' then
      self.repInfoFrame:show()
    elseif frame == 'ship' then
      self.shipInfoFrame:show()
    elseif frame == 'crew' then
      self.crewInfoFrame:show()
    end

  elseif message == 'next_day' then
    local dayInfo = data
    self.messagecenter:add('A day has passed...')
    if self.calendar:getToday() == 'Fri' then
      self.messagecenter:add('It\'s the weekend!')
    end

  elseif message == 'throw_party' then
    local port = data

    local partyData = self.partyThrower:throwPartyAtPort(port)

    -- TODO: Show party animation

    self.partyFrame:showForParty(partyData)

  end
end

function scene:setPaused(paused)
  self.paused = paused

  love.mouse.setGrabbed(not paused)

  if paused then
    self.pauseMenu:show()
  else
    self.pauseMenu:close()
  end
end

function scene:update(dt)
  Console:update(dt)
  Timer.update(dt)
  Tween.update(dt)
  Cursor:update(dt)

  self.logger:update(dt)
  self.gesturerecognizer:update(dt)

  if self.paused then
    dt = 0
  end

  dt = dt * self.timescale

  self.gesturerecognizer:update(dt)
  loveframes.update(dt)

  Notifier:postMessage('ui_update', dt)

  -- Handle mouse panning
  local mouse = vector(love.mouse.getX(), love.mouse.getY())
  local cam_movement = vector(0, 0)
  if mouse.x <= 0 or love.keyboard.isDown('left') then
    cam_movement = cam_movement + vector(-1, 0)
  end
  if mouse.x >= System:getWidth() - 1 or love.keyboard.isDown('right') then
    cam_movement = cam_movement + vector(1, 0)
  end
  if mouse.y <= 0 or love.keyboard.isDown('up') then
    cam_movement = cam_movement + vector(0, -1)
  end
  if mouse.y >= System:getHeight() - 1 or love.keyboard.isDown('down') then
    cam_movement = cam_movement + vector(0, 1)
  end

  if self.ship.velocity == vector(0, 0) then
    self.camera.smoothMovement = false
    self.camera:setMovement(cam_movement)
  else
    self.camera.smoothMovement = false
    self.camera.position = self.ship:getPosition()
  end

  self.camera:update(dt)

  local world = self.camera:screenToWorld(vector(love.mouse.getX(), love.mouse.getY()))
  self.logger:addLine('FPS: '..tostring(love.timer.getFPS()))
  self.logger:addLine('Screen: '..tostring(vector(love.mouse.getX(), love.mouse.getY())))
  self.logger:addLine('World: '..tostring(world))
  if self.world:pointIsWalkable(world) then
    self.logger:addLine('Walkable')
  end
  Cursor:set('cursor_pointer.png')

  for index, thing in ipairs(self.updatables) do
    thing:update(dt)
  end
end

function scene:draw()
  love.graphics.clear()
  colors.white:set()

  love.graphics.setCanvas(Canvases.buffer)
  love.graphics.clear(0, 0, 0)
  love.graphics.setShader()

  Sprites.main.batch:clear()

  self.camera:apply()

  self.world:draw()

  for index, entity in ipairs(self.drawables) do
    entity:draw()
  end

  self.ship:draw()

  colors.white:set()
  love.graphics.draw(Sprites.main.batch)

  self.camera:unapply()

  colors.white:set()
  loveframes.draw()

  Sprites.ui.batch:clear()
  Cursor:draw()
  love.graphics.draw(Sprites.ui.batch)

  love.graphics.setCanvas()
  Shaders:set(Shaders.currentEffect)

  love.graphics.draw(Canvases.buffer, Shaders.quad, 0, 0)
  love.graphics.setShader()


  Console:draw()
  self.logger:draw()

  love.graphics.print(string.format('Yachtventure! by Jay Roberts (build %s)', System.release), love.graphics.getWidth() - 430, 10)
end

function scene:quit()
  Gamestate.switch(scenes.title)
end

function scene:leave()
  print('leaving')
  Notifier:stopListeningForMessage('mouse_moved', self)
  Notifier:stopListeningForMessage('mouse_down', self)
  Notifier:stopListeningForMessage('mouse_down_right', self)
  Notifier:stopListeningForMessage('mouse_click', self)
  Notifier:stopListeningForMessage('world_down', self)
  Notifier:stopListeningForMessage('port_selected', self)
  Notifier:stopListeningForMessage('show_infoframe', self)
  Notifier:stopListeningForMessage('next_day', self)

  self:removeUI()
end

function scene:removeUI()
  if self.portInfoFrame then
    self.portInfoFrame:close()
  end
  if self.repInfoFrame then
    self.repInfoFrame = RepInfoFrame(self.player)
  end
  if self.shipInfoFrame then
    self.shipInfoFrame = ShipInfoFrame(self.ship)
  end
  if self.crewInfoFrame then
    self.crewInfoFrame = CrewInfoFrame()
  end
  if self.financeInfoFrame then
    self.financeInfoFrame = CrewInfoFrame()
  end
  if self.hud then
    self.hud:close()
  end
  if self.pauseMenu then
    self.pauseMenu:close()
  end
  if self.messageCenter then
    self.messagecenter:close()
  end
end


return scene