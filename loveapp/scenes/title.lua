--
--  base.lua
--  rogue-descent
--
--  A base scene that can be used when creating new scenes.
--
--  Created by Jay Roberts on 2012-02-23.
--  Copyright 2012 Jay Roberts. All rights reserved.
--

require 'logger'
require 'vector'
require 'colors'
require 'rectangle'

local scene = Gamestate.new()

function scene:enter(pre)
  self.logo = love.graphics.newImage('resources/sprites/logo.png')
  self.background = love.graphics.newImage('resources/sprites/titlebg.png')
  self.yacht = {
    sprite = love.graphics.newImage('resources/sprites/titleyacht.png'),
    position = vector(-70, 140),
    scale = 0.2,
  }

  Tween.start(8, self.yacht.position, { x = (System:getWidth() / 2) - 140, y = 150 }, 'inOutQuad')
  Tween.start(8, self.yacht, { scale = 0.8 })

  self.elapsed = 0
end

function scene:textinput(text)
end

function scene:keypressed(key, unicode)
  if key == 'escape' then
    self:quit()
  elseif key == 'f' then
    System:toggleFullscreen()
  else
    self:newGame()
  end
end

function scene:mousepressed(x, y, button)
end

function scene:mousereleased(x, y, button)
end

function scene:update(dt)
  self.elapsed = self.elapsed + dt
  Timer.update(dt)
  Tween.update(dt)
  Cursor:update(dt)
end

function scene:draw()
  love.graphics.clear()
  colors.white:set()

  love.graphics.setCanvas(Canvases.buffer)
  love.graphics.clear(255, 255, 255)
  love.graphics.setShader()

  colors.black:set()
  love.graphics.print('Press space to continue.', 30, 190)
  love.graphics.print('Press "F" to toggle fullscreen.', 30, 220)

  Sprites.ui.batch:clear()
  Cursor:draw()
  love.graphics.draw(Sprites.ui.batch)


  love.graphics.draw(self.background, 0, 0)


  local bob = math.sin(self.elapsed) * 2

  love.graphics.draw(self.yacht.sprite, self.yacht.position.x, self.yacht.position.y + bob * 5 * self.yacht.scale, 0, self.yacht.scale, self.yacht.scale)

  love.graphics.draw(self.logo, 10, 10)

  love.graphics.setCanvas()
  Shaders:set(Shaders.currentEffect)

  love.graphics.draw(Canvases.buffer, Shaders.quad, 0, 0)
  love.graphics.setShader()

  Console:draw()
end

function scene:newGame()
  Gamestate.switch(scenes.newgame)
end


function scene:quit()
  love.event.push('quit')
end

function scene:leave()
end

return scene