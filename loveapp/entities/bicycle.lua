-- Apple

require 'middleclass'
require 'actions/actions'

local EntityBicycle = class('EntityBicycle', Entity)


function EntityBicycle:initialize()
  Entity.initialize(self)

  self.animationName = 'entity_bicycle_idle'
end

function EntityBicycle:getAdvertisements()
  local ads = {
    {
      name = 'cycle',
      score = self.station:getPowerScore(),
      actions = {
        ActionMoveTo(self.position),
        ActionWait(2, 'Cycling'),
        ActionCustom('Cycle',
          function(this, dt) -- update
            this.agent:changeNeed('fatigue', -5)
            this.agent:changeNeed('boredom', -5)
            this.station:changePower(5)
            Notifier:postMessage('station_console_log', string.format('%s generated 5 power', this.agent:getName()))
          end,
          function() -- done
            return true
          end
         ),
      }
    },
  }

  return ads
end

function EntityBicycle:draw()
  Entity.draw(self)
end

return EntityBicycle