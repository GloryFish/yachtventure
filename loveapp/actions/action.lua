require 'middleclass'

Action = class('Action')

function Action:initialize()
  self.started = false
end

function Action:getName()
end

function Action:setAgent(agent)
  self.agent = agent
end

function Action:setStation(station)
  self.station = station
end


function Action:start()
end

function Action:update(dt)
end

function Action:isDone()
  return true
end