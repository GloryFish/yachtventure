require 'middleclass'
require  'actions/action'

ActionCustom = class('ActionCustom', Action)

function ActionCustom:initialize(name, update, done)
  Action.initialize(self)
  self.name = name
  self.update = update
  self.done = done
end

function ActionCustom:getName()
  return self.name
end