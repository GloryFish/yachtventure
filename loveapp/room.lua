require 'middleclass'
require 'vector'
require 'colors'
require 'rectangle'

Room = class('Room')

function Room:initialize(name, size)
  self.size = size
  self.rectangle = Rectangle(self.position, self.size)
  self.color = Color(255, 255, 255)
  self.name = name
  self.entities = {}
  self.active = false
end

function Room:setActive(active)
  self.active = active
end

function Room:getActive()
  return self.active
end

function Room:getSize()
  return self.size
end

function Room:setPosition(position)
  self.rectangle.position = position
end

function Room:getPosition()
  return self.rectangle.position
end

function Room:getCenter()
  return self.rectangle:center()
end

function Room:addEntity(entity)
  table.insert(self.entities, entity)
end

function Room:getEntities()
  return self.entities
end

function Room:containsLocation(location)
  return self.rectangle:containsPoint(vector(location, self.rectangle.position.y))
end

function Room:floorHeightAtPosition(horizontalPosition)
  local max = self.rectangle:getMax()
  return max.y - 30
end

function Room:draw()
  local min = self.rectangle:getMin()
  local max = self.rectangle:getMax()


  self.color:set()
  self.rectangle:draw(nil, 'fill')
  colors.black:set()
  self.rectangle:draw(nil, 'line')

  -- Draw room label
  love.graphics.print(self.name, min.x + 10, min.y + 10)

  -- Draw floor
  love.graphics.line(min.x, max.y - 30, max.x, max.y - 30)

  -- Draw entities
  for index, entity in ipairs(self.entities) do
    entity:draw()
  end

  if not self.active then
    colors.black:alpha(255 * 0.7):set()
    self.rectangle:draw(nil, 'fill')
  end
end
