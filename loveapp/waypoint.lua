--
--  waypoint.lua
--  rogue-descent
--
--  Created by Jay Roberts on 2012-02-27.
--  Copyright 2012 GloryFish.org. All rights reserved.
--

require 'middleclass'
require 'vector'
require 'circle'
require 'rectangle'



Waypoint = class('Waypoint')

Waypoint.current_id = 1

function Waypoint:initialize(position, id)
  if id == nil then
    self.id = tostring(Waypoint.current_id)
    Waypoint.current_id = Waypoint.current_id + 1
  else
    self.id = tostring(id)
    if id >= Waypoint.current_id then
      Waypoint.current_id = id + 1
    end
  end

  self.circle = Circle(position, 10)
  self.connections = {}
end

function Waypoint:__tostring()
  return 'Waypoint at '..tostring(self.circle.position)
end

function Waypoint:getPosition()
  return self.circle.position
end

function Waypoint:contains(point)
  return self.circle:contains(point);
end

function Waypoint:getRect()
  return Rectangle(vector(self.circle.position.x - self.circle.radius, self.circle.position.y - self.circle.radius),
                   vector(2 * self.circle.radius, 2 * self.circle.radius))
end

function Waypoint:connectTo(id)
  self.connections[id] = true
end

function Waypoint:removeConnectionTo(id)
  self.connections[id] = nil
end

function Waypoint:draw(selected)
  local selected = selected or false

  if selected then
    colors.yellow:set()
    self.circle:draw()
  else
    colors.red:set()
    self.circle:draw()
  end

  colors.white:set()
  love.graphics.printf(tostring(self.id), self.circle.position.x - self.circle.radius, self.circle.position.y - self.circle.radius + 3, 2 * self.circle.radius, 'center')
end