---------------------------------

-- panel class
local newobject = loveframes.NewObject("ratingsbar", "loveframes_object_ratingsbar", true)

--[[---------------------------------------------------------
  - func: initialize()
  - desc: initializes the object
--]]---------------------------------------------------------
function newobject:initialize()

  self.type = "ratingsbar"
  self.width = 200
  self.height = 50
  self.clickable = true
  self.enabled = true
  self.down = false
  self.internal = false
  self.children = {}

  self.max = 10
  self.value = 0

  self.style = 'regular'

end

--[[---------------------------------------------------------
  - func: update(deltatime)
  - desc: updates the element
--]]---------------------------------------------------------
function newobject:update(dt)

  local state = loveframes.state
  local selfstate = self.state

  if state ~= selfstate then
    return
  end

  local visible = self.visible
  local alwaysupdate = self.alwaysupdate

  if not visible then
    if not alwaysupdate then
      return
    end
  end

  self:CheckHover()

  local hover = self.hover
  local downobject = loveframes.downobject
  local down = self.down
  local children = self.children
  local parent = self.parent
  local base = loveframes.base
  local update = self.Update

  if not hover then
    self.down = false
  else
    if downobject == self then
      self.down = true
    end
  end

  if not down and downobject == self then
    self.hover = true
  end

  -- move to parent if there is a parent
  if parent ~= base and parent.type ~= "list" then
    self.x = self.parent.x + self.staticx
    self.y = self.parent.y + self.staticy
  end

  for k, v in ipairs(children) do
    v:update(dt)
  end

  if update then
    update(self, dt)
  end

end

--[[---------------------------------------------------------
  - func: draw()
  - desc: draws the object
--]]---------------------------------------------------------
function newobject:draw()

  local state = loveframes.state
  local selfstate = self.state

  if state ~= selfstate then
    return
  end

  local visible = self.visible

  if not visible then
    return
  end

  local children = self.children
  local skins = loveframes.skins.available
  local skinindex = loveframes.config["ACTIVESKIN"]
  local defaultskin = loveframes.config["DEFAULTSKIN"]
  local selfskin = self.skin
  local skin = skins[selfskin] or skins[skinindex]
  local drawfunc = skin.DrawRatingsBar or skins[defaultskin].DrawRatingsBar
  local draw = self.Draw
  local drawcount = loveframes.drawcount

  -- set the object's draw order
  self:SetDrawOrder()

  if draw then
    draw(self)
  else
    drawfunc(self)
  end

  -- loop through the object's children and draw them
  for k, v in ipairs(children) do
    v:draw()
  end

end

--[[---------------------------------------------------------
  - func: mousepressed(x, y, button)
  - desc: called when the player presses a mouse button
--]]---------------------------------------------------------
function newobject:mousepressed(x, y, button)

  local state = loveframes.state
  local selfstate = self.state
  local clickable = self.clickable

  if state ~= selfstate then
    return
  end

  local visible = self.visible

  if not visible then
    return
  end

  local children = self.children
  local hover = self.hover

  if hover and button == 1 and clickable then
    local baseparent = self:GetBaseParent()
    if baseparent and baseparent.type == "frame" then
      baseparent:MakeTop()
    end
    self.down = true
    loveframes.downobject = self
  end

  for k, v in ipairs(children) do
    v:mousepressed(x, y, button)
  end

end

--[[---------------------------------------------------------
  - func: mousereleased(x, y, button)
  - desc: called when the player releases a mouse button
--]]---------------------------------------------------------
function newobject:mousereleased(x, y, button)

  local state = loveframes.state
  local selfstate = self.state

  if state ~= selfstate then
    return
  end

  local hover = self.hover
  local down = self.down
  local clickable = self.clickable
  local enabled = self.enabled
  local visible  = self.visible
  local onclick = self.OnClick
  local children = self.children

  if not visible then
    return
  end

  if hover and down and clickable and button == 1 then
    if enabled then
      if onclick then
        onclick(self, x, y)
      end
    end
  end

  self.down = false

  for k, v in ipairs(children) do
    v:mousereleased(x, y, button)
  end

end

function newobject:SetValue(value)
  if value < 0 then
    value = 0
  end
  if value > self.max then
    value = self.max
  end
  self.value = value
end

function newobject:GetValue()
  return self.value
end

function newobject:SetMax(max)
  self.max = max
end

function newobject:GetMax()
  return self.max
end

--[[---------------------------------------------------------
  - func: SetClickable(bool)
  - desc: sets whether the object can be clicked or not
--]]---------------------------------------------------------
function newobject:SetClickable(bool)

  self.clickable = bool
  return self

end

--[[---------------------------------------------------------
  - func: GetClickable(bool)
  - desc: gets whether the object can be clicked or not
--]]---------------------------------------------------------
function newobject:GetClickable()

  return self.clickable

end

function newobject:SetStyle(style)
  self.style = style
end

function newobject:GetStyle()
  return self.style
end



