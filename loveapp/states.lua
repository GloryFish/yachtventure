require 'middleclass'

local States = class('States')

function States:initialize()
  -- loop through states
  self.states = {}
  local statelist = love.filesystem.getDirectoryItems('states')

  for i, filename in ipairs(statelist) do
    local name = filename:sub(1, -5)
    local ext = filename:sub(-3)
    if ext == 'lua' then
      self.states[name] = require('states/'..name)
    end
  end
end

function States:getState(name)
  return self.states[name]()
end

return States()