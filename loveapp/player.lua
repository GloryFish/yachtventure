require 'middleclass'
require 'vector'

Player = class('Player')

function Player:initialize()
  self.name = ''
  self.baseIncome = 1000
  self.reputation = 0
end

function Player:setName(name)
  self.name = name
end

function Player:getName()
  return self.name
end

function Player:setBaseIncome(amount)
  self.baseIncome = amount
end

function Player:addBaseIncome(amount)
  self.baseIncome = self.baseIncome + amount
end

function Player:getBaseIncome()
  return self.baseIncome
end

function Player:getAggregateIncome()
  assert(false, 'not yet implemented')
  return
end

function Player:setReputation(reputation)
  self.reputation = reputation
end

function Player:getReputation()
  return self.reputation
end
