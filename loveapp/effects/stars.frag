// Noise generation functions borrowed from:
// https://github.com/ashima/webgl-noise/blob/master/src/noise2D.glsl

vec3 mod289(vec3 x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec2 mod289(vec2 x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec3 permute(vec3 x) {
  return mod289(((x*34.0)+1.0)*x);
}

float snoise(vec2 v) {
  const vec4 C = vec4(0.211324865405187,  // (3.0-sqrt(3.0))/6.0
                      0.366025403784439,  // 0.5*(sqrt(3.0)-1.0)
                     -0.577350269189626,  // -1.0 + 2.0 * C.x
                      0.024390243902439); // 1.0 / 41.0
  // First corner
  vec2 i  = floor(v + dot(v, C.yy) );
  vec2 x0 = v -   i + dot(i, C.xx);

  // Other corners
  vec2 i1;
  //i1.x = step( x0.y, x0.x ); // x0.x > x0.y ? 1.0 : 0.0
  //i1.y = 1.0 - i1.x;
  i1 = (x0.x > x0.y) ? vec2(1.0, 0.0) : vec2(0.0, 1.0);
  // x0 = x0 - 0.0 + 0.0 * C.xx ;
  // x1 = x0 - i1 + 1.0 * C.xx ;
  // x2 = x0 - 1.0 + 2.0 * C.xx ;
  vec4 x12 = x0.xyxy + C.xxzz;
  x12.xy -= i1;

  // Permutations
  i = mod289(i); // Avoid truncation effects in permutation
  vec3 p = permute( permute( i.y + vec3(0.0, i1.y, 1.0 ))
    + i.x + vec3(0.0, i1.x, 1.0 ));

  vec3 m = max(0.5 - vec3(dot(x0,x0), dot(x12.xy,x12.xy), dot(x12.zw,x12.zw)), 0.0);
  m = m*m ;
  m = m*m ;

  // Gradients: 41 points uniformly over a line, mapped onto a diamond.
  // The ring size 17*17 = 289 is close to a multiple of 41 (41*7 = 287)

  vec3 x = 2.0 * fract(p * C.www) - 1.0;
  vec3 h = abs(x) - 0.5;
  vec3 ox = floor(x + 0.5);
  vec3 a0 = x - ox;

  // Normalise gradients implicitly by scaling m
  // Approximation of: m *= inversesqrt( a0*a0 + h*h );
  m *= 1.79284291400159 - 0.85373472095314 * ( a0*a0 + h*h );

  // Compute final noise value at P
  vec3 g;
  g.x  = a0.x  * x0.x  + h.x  * x0.y;
  g.yz = a0.yz * x12.xz + h.yz * x12.yw;
  return 130.0 * dot(m, g);
}

// Random value used for static noise generation
float rand(vec2 co){
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

extern vec2 textureSize;
extern float globalTime;
extern float amount;

vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 pixel_coords)
{
  float distortAmount = amount * amount * amount * amount; // Ease the distortion amount for finer control at the low end

  vec2 uv = texture_coords;

  float jerkOffset = (1.0 - step(snoise(vec2(globalTime * 1.3, 5.0)), 0.8)) * 0.05;
  float wiggleOffset = snoise(vec2(globalTime * 15.0, uv.y * 80.0)) * 0.003;
  float largeWiggleOffset = snoise(vec2(globalTime * 1.0, uv.y * 25.0)) * 0.004;

  // Horizontal warping
  float xOffset = (wiggleOffset + largeWiggleOffset + jerkOffset) * distortAmount * 20.0;

  // RGB channel offset
  float red   = Texel(texture,  vec2(uv.x + xOffset - (0.025 * distortAmount), uv.y)).r;
  float green = Texel(texture,  vec2(uv.x + xOffset,                   uv.y)).g;
  float blue  = Texel(texture,  vec2(uv.x + xOffset + (0.025 * distortAmount), uv.y)).b;

  // Scanlines
  vec3 outputColor = vec3(red, green, blue);
  float scanline = sin(uv.y * 800.0) * 0.04;
  outputColor -= scanline;

  // Static
  float screenRatio = textureSize.x / textureSize.y;

  float barHeight = 6.;
  float barSpeed = 3.6;
  float barOverflow = 1.2;
  float blurBar = clamp(sin(uv.y * barHeight + globalTime * barSpeed) + 1.25, 0., 1.);
  float bar = clamp(floor(sin(uv.y * barHeight + globalTime * barSpeed) + 1.95), 0., barOverflow);

  float noiseIntensity = 1.0;
  float pixelDensity = 500.0;
  vec3 staticColor = vec3(clamp(rand(vec2(floor(uv.x * pixelDensity * screenRatio), floor(uv.y * pixelDensity)) * globalTime / 1000.0  ) + 1.0 - noiseIntensity, 0.0, 1.0));

  staticColor = mix(staticColor - noiseIntensity * vec3(.25), staticColor, blurBar);
  staticColor = mix(staticColor - noiseIntensity * vec3(.08), staticColor, bar);
  staticColor = mix(outputColor, vec3(0.0),  staticColor * distortAmount * 0.5);
  staticColor.b += .042;

  // Vignette
  staticColor *= vec3(1.0 - pow(distance(uv, vec2(0.5, 0.5)), 2.1) * 2.8 * distortAmount);

  return vec4(staticColor, 1.0);
}
