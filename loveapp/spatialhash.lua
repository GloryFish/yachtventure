--
--  door.lua
--  rogue-descent
--
--  Created by Jay Roberts on 2012-02-23.
--  Copyright 2012 Jay Roberts. All rights reserved.
--

require 'middleclass'
require 'vector'
require 'rectangle'
require 'colors'
require 'utility'
require 'notifier'
require 'rectangle'
require 'notifier'
require 'circle'

SpatialHash = class('SpatialHash')

function SpatialHash:initialize(cell_size)
  assert(cell_size ~= nil, 'SpatialHash initialized without cell_size')
  self.cell_size = cell_size
  self.contents = {}
end

function SpatialHash:hash(point)
  return math.floor(point.x / self.cell_size), math.floor(point.y / self.cell_size)
end


function SpatialHash:insertObjectForPoint(object, point)
  local hashx, hashy = self:hash(point)

  if not self.contents[hashx] then
    self.contents[hashx] = {}
  end

  if not self.contents[hashx][hashy] then
    self.contents[hashx][hashy] = {}
  end

  table.insert(self.contents[hashx][hashy], object)
end

function SpatialHash:insertObjectForRect(object, rect)
  local hashmin_x, hashmin_y = self:hash(rect:getMin())
  local hashmax_x, hashmax_y = self:hash(rect:getMax())

  for i = hashmin_x, hashmax_x do
    for j = hashmin_y, hashmax_y do
      if not self.contents[i] then
        self.contents[i] = {}
      end

      if not self.contents[i][j] then
        self.contents[i][j] = {}
      end

      table.insert(self.contents[i][j], object)
    end
  end
end

function SpatialHash:removeObjectForPoint(object, point)
  local hashx, hashy = self:hash(point)
  if self.contents[hashx] and self.contents[hashx][hashy] then
    for index, o in ipairs(self.contents[hashx][hashy]) do
      if o == object then
        table.remove(self.contents[hashx][hashy], index)
        return
      end
    end
  end
end

function SpatialHash:removeObjectForRect(object, rect)
  local objects = {}
  local hashmin_x, hashmin_y = self:hash(rect:getMin())
  local hashmax_x, hashmax_y = self:hash(rect:getMax())

  for i = hashmin_x, hashmax_x do
    for j = hashmin_y, hashmax_y do
      if self.contents[i] and self.contents[i][j] then
        for k = 1, #self.contents[i][j] do
          if self.contents[i][j][k] == object then
            table.remove(self.contents[i][j], k)
          end
        end
      end
    end
  end
end

function SpatialHash:objectsForPoint(point)
  local hashx, hashy = self:hash(point)
  if self.contents[hashx] and self.contents[hashx][hashy] then
    return self.contents[hashx][hashy]
  else
    return {}
  end
end


function SpatialHash:objectsForRect(rect)
  local objects = {}
  local hashmin_x, hashmin_y = self:hash(rect:getMin())
  local hashmax_x, hashmax_y = self:hash(rect:getMax())

  for i = hashmin_x, hashmax_x do
    for j = hashmin_y, hashmax_y do
      if self.contents[i] and self.contents[i][j] then
        for _, object in pairs(self.contents[i][j]) do
          rawset(objects, object, object)
        end
      end
    end
  end

  return objects
end
