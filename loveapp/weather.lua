require 'middleclass'
require 'notifier'

Weather = class('Weather')

local weather_options = {
  'Stormy',
  'Cloudy',
  'Fair',
  'Sunny',
}

function Weather:initialize()
  self.maxDays = 5

  self.forecast = {}

  Notifier:listenForMessage('next_day', self)

  self:generateForecast()
end

function Weather:getForecast()
  if #self.forecast < self.maxDays then
    self:generateForecast()
  end
  return self.forecast
end

function Weather:getToday()
  if #self.forecast < self.maxDays then
    self:generateForecast()
  end
  return self.forecast[1]
end

function Weather:generateForecast()
  while #self.forecast < self.maxDays do
    table.insert(self.forecast, weather_options[math.random(#weather_options)])
  end
end

function Weather:receiveMessage(message, data)
  if message == 'next_day' then
    table.remove(self.forecast, 1)
    self:generateForecast()
    Notifier:postMessage('weather_changed', self.forecast)
  end
end