--
--  circle.lua
--

require 'middleclass'
require 'circle'
require 'vector'
require 'colors'

Label = class('Label')

function Label:initialize(font)
  self.font = font
  self.text = ''
  self.anchor = vector(0.5, 0.5)
  self.position = vector(0, 0)
  self.rotation = 0
  self.color = colors.white
  self.shadowOffset = vector(1, 1)
  self.shadowColor = colors.black
end


function Label:draw()
  love.graphics.setFont(self.font)
  local w, h = self.font:getWidth(self.text), self.font:getHeight()

  local textPosition = vector(self.position.x - w * self.anchor.x, self.position.y - h * self.anchor.y)

  self.shadowColor:set()
  love.graphics.print(self.text,
                      math.floor(textPosition.x + self.shadowOffset.x),
                      math.floor(textPosition.y + self.shadowOffset.y))

  self.color:set()
  love.graphics.print(self.text,
                      math.floor(textPosition.x),
                      math.floor(textPosition.y))


end




