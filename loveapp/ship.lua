require 'middleclass'
require 'notifier'

Ship = class('Ship')

local ships = require 'resources/data/ships'

function Ship:initialize(model)
  local shipdata = ships[model]
  for key, value in pairs(shipdata) do
    self[key] = value
  end

  -- Size and position
  self.position = vector(0, 0)
  self.displayPosition = vector(0, 0)
  self.rotation = 0
  self.scale = 1
  self.flip = -1
  self.offset = vector(24, 16)
  self.direction = vector(0, -1)

  -- Movement
  self.movement = vector(0, 0) -- This holds a vector containing the last movement input received
  self.velocity = vector(0, 0)

  -- Pathfinding
  self.target = nil
  self.path = nil
  self.arrivalDistance = 3

  -- Stats
  self.vision_range = 40
  self.base_speed = self.speed * 10
  self.durability = self.maxDurability

  -- Animation
  self.currentFrameIndex = 1
  self.animationName = string.format('ship_%s_idle', model)
  self.animationElapsed = 0

  self.spritesheet = Sprites.main

  self.state = States:getState('idle')
  self:setState(States:getState('idle'))

  -- Time passage
  self.distanceTravelledToday = 0

  -- We determine the number of units a given ship can travel per day based on its speed.
  -- The fastest ships (5) should be able to travel 250 units per day.
  -- The slowest ships (1) should be able to travel 50 units in a day.
  self.unitsPerDay = self.speed * 50
end

function Ship:getName()
  return self.name
end

function Ship:getPosition()
  return self.position
end

function Ship:setPosition(position)
  self.position = position
end

-- Call during update with a normalized movement vector
function Ship:setMovement(movement)
  self.movement = movement
  self.velocity.x = movement.x * self.base_speed
  self.velocity.y = movement.y * self.base_speed

  if movement.x ~= 0 and movement.y ~= 0 then
    self.direction = self.movement
  end

  if movement.x > 0 then
    self.flip = -1
  end

  if movement.x < 0 then
    self.flip = 1
  end
end

function Ship:setAnimation(name)
  if (self.animationName ~= name) then
    self.animationName = name
    self.currentFrameIndex = 1
    self.animationElapsed = 0
  end
end

function Ship:getCurrentSize()
  local animation = self.spritesheet.animations[self.animationName]
  local currentFrame = animation.frames[self.currentFrameIndex]
  local quad = self.spritesheet.quads[currentFrame.name]
  local x, y, w, h = quad:getViewport()
  return vector(w, h)
end

function Ship:setState(state)
  if (self.state ~= state) then
    self.state:exit()
    self.state = state
    self.state:enter(self)
  end
end

function Ship:setTargetPoint(target)
  self.target = target
  self.path = nil
  self:setState(States:getState('move_to_target'))
end

function Ship:getAIMovement(target, map)
  local movement = vector(0, 0)

  if self.path == nil then
    self.path = map:pathBetween(self.position, target, self.vision_range)
    if self.path == nil then
      return vector(0, 0)
    end
  end

  local nextLocation = self.path[1]

  -- Check to see if we've reached the current node
  if nextLocation ~= nil then
    if self.position:dist(nextLocation) < self.arrivalDistance then
      table.remove(self.path, 1)
      nextLocation = self.path[1]
    end
  end

  -- Move towards our current node
  if nextLocation ~= nil then
    local movement = nextLocation - self.position
    movement:normalize_inplace()
    return movement
  else
      self.path = nil
      return vector(0, 0)
  end

  return movement
end

function Ship:repair()
  self.durability = self.maxDurability
end

function Ship:hasSupplies()
  return true
end

function Ship:getScore()
  return 5
end

function Ship:update(dt)
  self.state:update(dt)

  -- Increment day if we've traveled as far as unitsPerDay
  self.distanceTravelledToday = self.distanceTravelledToday + self.velocity:len() * dt
  if self.distanceTravelledToday > self.unitsPerDay then
    self.distanceTravelledToday = self.distanceTravelledToday - self.unitsPerDay
    Notifier:postMessage('next_day')
  end

  -- Apply velocity to position
  self.position = self.position + self.velocity * dt

  -- Handle animation
  self.animationElapsed = self.animationElapsed + dt
  local animation = self.spritesheet.animations[self.animationName]
  if #animation.frames > 1 then -- More than one frame
    local duration = animation.frames[self.currentFrameIndex].duration

    if self.animationElapsed > duration then -- Switch to next frame
      self.currentFrameIndex = self.currentFrameIndex + 1
      if self.currentFrameIndex > #animation.frames then -- Aaaand back around
        self.currentFrameIndex = 1
      end
      self.animationElapsed = self.animationElapsed - duration
    end
  end
end

function Ship:draw()
  local animation = self.spritesheet.animations[self.animationName]
  local currentFrame = animation.frames[self.currentFrameIndex]
  self.spritesheet.batch:add(self.spritesheet.quads[currentFrame.name],
                              math.floor(self.displayPosition.x),
                              math.floor(self.displayPosition.y),
                              animation.rotation - self.rotation,
                              self.scale * self.flip,
                              self.scale,
                              self.offset.x,
                              self.offset.y)

  if vars.showai then
    if self.path then
      colors.black:set()



      for index, point in ipairs(self.path) do
        love.graphics.circle('fill', point.x, point.y, 3, 10)
        if index == 1 then
          love.graphics.line(self.position.x, self.position.y, point.x, point.y)
        else
          love.graphics.line(self.path[index - 1].x, self.path[index -1].y, point.x, point.y)
        end
      end
    end

    colors.black:alpha(100):set()
    love.graphics.circle('line', self.displayPosition.x, self.displayPosition.y, self.vision_range, 60)
  end
end