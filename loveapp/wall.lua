--
--  wall.lua
--  rogue-descent
--
--  Created by Jay Roberts on 2012-02-23.
--  Copyright 2012 Jay Roberts. All rights reserved.
--

require 'middleclass'
require 'vector'
require 'rectangle'
require 'colors'
require 'utility'
require 'notifier'
require 'rectangle'
require 'notifier'

Wall = class('Wall')

function Wall:initialize(position, size)
  assert(position ~= nil, 'Wall initialized without position')
  assert(position ~= nil, 'Wall initialized without size')

  self.rectangle = Rectangle(position, size)

  self.color = colors.gray
end

function Wall:draw(color)
  if color then
    color:set()
  else
    self.color:set()
  end
  self.rectangle:draw(vector(0, 0), 'fill')
end