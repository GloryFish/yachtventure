local animations = {
  ship_envynautilus_idle = {
    rotation = 0,
    frames = {
      {
        name = 'ship_envynautilus_idle_1.png',
        duration = 1.5,
      },
      {
        name = 'ship_envynautilus_idle_2.png',
        duration = 1.5,
      },
    },
  },
  ship_sunblaster_idle = {
    rotation = 0,
    frames = {
      {
        name = 'ship_sunblaster_idle.png',
        duration = 5,
      },
    },
  },
}

return animations