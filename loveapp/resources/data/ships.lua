local ships = {
  sunblaster = {
    model = 'sunblaster',
    name = 'Sunblaster 44',
    manufacturer = 'Manta Ray Boatworks',
    length = 44,
    year = 2004,
    description = 'A single deck craft ideal for short trips with a few close friends.',
    speed = 1,
    maxcrew = 2,
    maxguest = 2,
    sail = false,
    maxDurability = 1000,
  },
  envynautilus = {
    model = 'envynautilus',
    name = 'Envy Nautilus',
    manufacturer = 'Oceanic vanity',
    length = 44,
    year = 2004,
    description = 'A single deck craft ideal for short trips with a few close friends.',
    speed = 5,
    maxcrew = 2,
    maxguest = 2,
    sail = false,
    maxDurability = 5000,
  },
}

return ships