-- Max desciprion size:
-- 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate.'

local ports = {
  port_freshportmarina = {
    name = 'Freshport Marina',
    description = 'Features two dozen wet slips, a payphone, and is conveniently located within walking distance of a laundromat and an Applebee\'s. What more could a new captain want?\n\nWell...a lot actually.',
    reputationStandard = 0,
    partyThreshold = 10,
    activities = {
      'wait',
      'party',
    },
    attendeeMin = 0,
    attendeeMax = 10,
  },
  port_phasungbeach = {
    name = 'Phasung Beach',
    reputationStandard = 3,
    partyThreshold = 20,
    activities = {
      'wait',
      'party',
      'repair',
    },
    attendeeMin = 0,
    attendeeMax = 10,
  },
  port_eastbay = {
    name = 'East Bay',
    reputationStandard = 7,
    partyThreshold = 20,
    activities = {
      'wait',
      'party',
      'repair',
    },
    attendeeMin = 0,
    attendeeMax = 10,
  },
  port_blackwatercove = {
    name = 'Blackwater Cove',
    description = 'A secluded inlet surrounded by high cliffs. The water here is actually crystal clear. It\'s a perfect place for an intimate soiree.',
    reputationStandard = 3,
    partyThreshold = 50,
    activities = {
      'wait',
      'party',
    },
    attendeeMin = 0,
    attendeeMax = 10,
  },
}

return ports