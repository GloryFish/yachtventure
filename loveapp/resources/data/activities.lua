require 'middleclass'
require 'notifier'




local activities = {
  remote = {
    go = {
      name = 'Go',
      getDescription = function(context)
        return 'Travel to this port.'
      end,
      isAvailable = function(context)
        return true
      end,
      run = function(context)
        context.ship:setTargetPoint(context.port:getPosition())
      end,
    },
  },

  port = {
    wait = {
      name = 'Wait',
      getDescription = function(context)
        return 'Chill here for a day.'
      end,
      isAvailable = function(context)
        return context.port:isInVicinity(context.ship:getPosition())
      end,
      run = function(context)
        Notifier:postMessage('next_day')
      end,
    },
    party = {
      name = 'Party!',
      getDescription = function(context)
        if context.port:isInVicinity(context.ship:getPosition()) and context.weather:getToday() == 'Stormy' then
          return 'You can\'t party in Stormy weather'
        else
          return 'Throw a party here.'
        end
      end,
      isAvailable = function(context)
        return context.port:isInVicinity(context.ship:getPosition()) and context.weather:getToday() ~= 'Stormy'
      end,
      run = function(context)
        Notifier:postMessage('throw_party', context.port)
      end,
    },
    repair = {
      name = 'Repair',
      getDescription = function(context)
        return 'Fully repair your yacht. Takes one day.'
      end,
      isAvailable = function(context)
        return context.port:isInVicinity(context.ship:getPosition())
      end,
      run = function(context)
        context.ship:repair()
        Notifier:postMessage('next_day')
      end,
    },
  },

  ship = {
  },

  crew = {
  },
}

return activities
