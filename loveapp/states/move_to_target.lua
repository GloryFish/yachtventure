require 'middleclass'
require 'vector'

local StateMoveToTarget = class('StateMoveToTarget')

function StateMoveToTarget:initialize()
end

function StateMoveToTarget:enter(actor)
  self.actor = actor

  self.displayTimer = Timer.addPeriodic(0.3, function()
    actor.displayPosition = actor.position
  end)
end

function StateMoveToTarget:update(dt)
  local actor = self.actor
  if actor.target == nil then
    actor:setState(States:getState('idle'))
  elseif actor.target:dist(actor.position) < actor.arrivalDistance then
    actor:setState(States:getState('idle'))
  else
    actor:setMovement(actor:getAIMovement(actor.target, actor.world))
  end
end

function StateMoveToTarget:exit()
  Timer.cancel(self.displayTimer)
  self.displayTimer = nil
end

return StateMoveToTarget