require 'middleclass'

local StateIdle = class('StateIdle')

function StateIdle:initalize()
end

function StateIdle:enter(actor)
  self.actor = actor
  self.actor:setMovement(vector(0, 0))
  print("idling")
end

function StateIdle:update(dt)
  self.actor.displayPosition = self.actor.position
end

function StateIdle:exit()
end

return StateIdle
