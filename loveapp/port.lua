require 'middleclass'
require 'circle'
require 'label'

Port = class('Port')

local ports = require 'resources/data/ports'

function Port:initialize(position, identfier)
  assert(ports[identfier], 'Missing portdata for '..identfier)

  local portdata = ports[identfier]
  for key, value in pairs(portdata) do
    self[key] = value
  end

  -- Entity values
  self.id = identfier

  self.position = position + vector(8, -8)

  self.radius = 6
  self.circle = Circle(self.position, self.radius)
  self.vicinity = Circle(self.position, self.radius + 10)

  self.nameLabel = Label(fonts.maplabel)
  self.nameLabel.text = self.name
  self.nameLabel.position = self.position + vector(0, 9)

  Notifier:listenForMessage('world_moved', self)
  Notifier:listenForMessage('world_down', self)
end

function Port:receiveMessage(message, data)
  if message == 'world_moved' then
    local position = data

    local contains = self:contains(position)

    if contains and not self.highlighted then
      Notifier:postMessage('port_highlighted', self)
    end

    self.highlighted = contains
  elseif message == 'world_down' then
    local position = data
    if self:contains(position) then
      Notifier:postMessage('port_selected', self)
    end
  end
end

function Port:getPosition()
  return self.position
end

function Port:update(dt)
end

function Port:getID()
  return self.id
end

function Port:getName()
  return self.name
end

function Port:getDescription()
  return self.description or 'No description available.'
end


function Port:getReputationStandard()
  return self.reputationStandard
end

function Port:contains(point)
  return self.circle:contains(point)
end

function Port:isInVicinity(point)
  return self.vicinity:contains(point)
end


function Port:draw()
  self.nameLabel:draw()

  if vars.showwalls then
    colors.yellow:set()
    self.circle:draw()
  end

  if self.highlighted then
    colors.white:set()
    self.circle:draw()
  end
end