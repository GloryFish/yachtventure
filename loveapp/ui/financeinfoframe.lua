require 'vector'
require 'middleclass'
require 'ui/infoframe'

FinanceInfoFrame = class('FinanceInfoFrame', InfoFrame)


function FinanceInfoFrame:initialize(world, player)
  InfoFrame.initialize(self)
  self.player = player
end

function FinanceInfoFrame:show()
  InfoFrame.show(self)

  self.frame:SetName('Financial Details')
end

