require 'vector'
require 'middleclass'
require 'ui/infoframe'

CrewInfoFrame = class('CrewInfoFrame', InfoFrame)

function CrewInfoFrame:initialize(world, player)
  InfoFrame.initialize(self)
  self.player = player
end

function CrewInfoFrame:show()
  InfoFrame.show(self)

  self.frame:SetName('Crew Details')
end
