require 'vector'
require 'middleclass'

PauseMenu = class('PauseMenu')


function PauseMenu:initialize(game)
  -- UI components
  self.frame = nil
  self.game = game
end

function PauseMenu:receiveMessage(message, data)
  if message == 'ui_update' then
    self:update(data)
  elseif message == 'window_reload' then
    self.frame:CenterWithinArea(0, 0, System:getWidth(), System:getHeight())
  end
end

function PauseMenu:close()
  if self.frame then
    self.frame:Remove()
  end
  self.frame = nil
  Notifier:stopListeningForMessage('ui_update', self)
end

function PauseMenu:show()
  if self.frame then
    self.frame:Remove()
  end
  local frame = loveframes.Create('frame')

  frame:SetModal(true)
  frame:ShowCloseButton(true)
  frame:SetDraggable(false)
  frame:SetSize(160, 170)
  frame:CenterWithinArea(0, 0, System:getWidth(), System:getHeight())
  frame:SetName("Paused")

  frame.OnClose = function()
    self:close()
  end

  self.frame = frame

  local saveButton = loveframes.Create('button', frame)
  saveButton:SetSize(34, 16)
  saveButton:SetPos(5, 25)
  saveButton:SetText('Save')
  saveButton.OnClick = function()
    print('TODO: Save game')
  end

  local saveLabel = loveframes.Create('text', frame)
  saveLabel:SetPos(43, 28)
  saveLabel:SetText('Last saved: never')
  saveLabel:SetFont(fonts.hud)


  local quitToMenuButton = loveframes.Create('button', frame)
  quitToMenuButton:SetSize(65, 16)
  quitToMenuButton:SetPos(5, 45)
  quitToMenuButton:SetText('Quit to Menu')
  quitToMenuButton.OnClick = function()
    self.game:quit()
  end

  local quitToDesktopButton = loveframes.Create('button', frame)
  quitToDesktopButton:SetSize(80, 16)
  quitToDesktopButton:CenterX()
  quitToDesktopButton:SetPos(75, 45)
  quitToDesktopButton:SetText('Quit to Desktop')
  quitToDesktopButton.OnClick = function()
    love.event.push('quit')
  end

  local fullscreenButton = loveframes.Create('button', frame)
  fullscreenButton:SetSize(80, 16)
  fullscreenButton:CenterX()
  fullscreenButton:SetY(65)
  fullscreenButton:SetText('Set Fullscreen')
  if System.fullscreen then
    fullscreenButton:SetText('Set Windowed')
  end
  fullscreenButton.OnClick = function()
    System:toggleFullscreen()
    fullscreenButton:SetText('Set Fullscreen')
    if System.fullscreen then
      fullscreenButton:SetText('Set Windowed')
    end
  end

  local soundLabel = loveframes.Create('text', frame)
  soundLabel:SetText('Sound Volume')
  soundLabel:SetPos(5, 90)
  soundLabel:SetFont(fonts.hud)

  local soundSlider = loveframes.Create('slider', frame)
  soundSlider:SetPos(5, 100)
  soundSlider:SetWidth(150)
  soundSlider:SetMinMax(0, 100)


  local musicLabel = loveframes.Create('text', frame)
  musicLabel:SetText('Music Volume')
  musicLabel:SetPos(5, 130)
  musicLabel:SetFont(fonts.hud)

  local musicSlider = loveframes.Create('slider', frame)
  musicSlider:SetPos(5, 140)
  musicSlider:SetWidth(150)
  musicSlider:SetMinMax(0, 100)


  Notifier:listenForMessage('ui_update', self)
  Notifier:listenForMessage('window_reload', self)
end

function PauseMenu:update(dt)
end

