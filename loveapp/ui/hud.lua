require 'vector'
require 'middleclass'
require 'colors'
require 'utility'

Hud = class('Hud')


function Hud:initialize(ship, player, weather, calendar, game)
  -- Game objects
  self.ship = ship
  self.player = player
  self.weather = weather
  self.calendar = calendar
  self.game = game

  -- UI elements
  self.panel = nil
  self.incomeText = nil;
  self.repBar = nil

  self.weatherIcons = {}
  self.weatherIcons['Sunny'] = love.graphics.newImage('resources/sprites/weather_sunny.png')
  self.weatherIcons['Fair'] = love.graphics.newImage('resources/sprites/weather_fair.png')
  self.weatherIcons['Cloudy'] = love.graphics.newImage('resources/sprites/weather_cloudy.png')
  self.weatherIcons['Stormy'] = love.graphics.newImage('resources/sprites/weather_stormy.png')

  Notifier:listenForMessage('ui_update', self)
  Notifier:listenForMessage('window_reload', self)
  Notifier:listenForMessage('weather_changed', self)
  Notifier:listenForMessage('calendar_changed', self)
end

function Hud:show()
  local panel = loveframes.Create('panel')
  panel:SetSize(System:getWidth() + 2, 24)
  panel:SetPos(-1, System:getHeight() - 24 + 1)
  self.panel = panel

  self.incomeText = loveframes.Create('text', panel)
  self.incomeText:SetPos(4, 14)
  self.incomeText:SetFont(fonts.hud)

  local repText = loveframes.Create('text', panel)
  repText:SetPos(4, 4)
  repText:SetFont(fonts.hud)
  repText:SetText('Rep:')

  self.repBar = loveframes.Create('ratingsbar', panel)
  self.repBar:SetPos(22, 3)
  self.repBar:SetStyle('small')
  self.repBar.OnClick = function()
    Notifier:postMessage('show_infoframe', 'reputation')
  end

  -- Add weather icons
  self.weatherImages = {}
  for i = 1, #self.weather:getForecast() do
    local image = loveframes.Create('image', panel)
    table.insert(self.weatherImages, image)
  end
  self:layoutWeather()
  self:setWeatherData(self.weather:getForecast())

  -- Add calendar labels
  self.calendarLabels = {}
  for i = 1, #self.weather:getForecast() do
    local text = loveframes.Create('text', panel)
    text:SetFont(fonts.hud)
    table.insert(self.calendarLabels, text)
  end
  self:layoutCalendar()
  local week = self.calendar:getWeek()
  self:setCalendarData(week)

  -- Add buttons
  self.buttons = {}

  local button = loveframes.Create('button', panel)
  button:SetSize(34, 16)
  button:SetText('Ship')
  button.OnClick = function()
    Notifier:postMessage('show_infoframe', 'ship')
  end
  table.insert(self.buttons, button)

  button = loveframes.Create('button', panel)
  button:SetSize(34, 16)
  button:SetText('Crew')
  button.OnClick = function()
    Notifier:postMessage('show_infoframe', 'crew')
  end
  table.insert(self.buttons, button)

  button = loveframes.Create('button', panel)
  button:SetSize(34, 16)
  button:SetText('Menu')
  button.OnClick = function()
      self.game:setPaused(true)
  end
  table.insert(self.buttons, button)

  self:layoutButtons()


end

function Hud:layoutWeather()
  local padding = 3
  local totalWidth = 16 * 7 + 6 * padding
  local startX = (self.panel:GetWidth() / 2 - totalWidth / 2) + 15

  for index, image in ipairs(self.weatherImages) do
    image:SetY(3)
    image:SetX(math.floor(startX + (index - 1) * (16 + padding)))
  end
end

function Hud:setWeatherData(forecast)
  for index, weather in ipairs(forecast) do
    self.weatherImages[index]:SetImage(self.weatherIcons[weather])
  end
end

function Hud:layoutCalendar()
  local padding = 3
  local totalWidth = 16 * 7 + 6 * padding
  local startX = (self.panel:GetWidth() / 2 - totalWidth / 2) + 16

  for index, text in ipairs(self.calendarLabels) do
    text:SetY(14)
    text:SetX(math.floor(startX + (index - 1) * (16 + padding)))
  end
end

function Hud:setCalendarData(week)
  for index, day in ipairs(week) do
    if self.calendarLabels[index] then
      if day == 'Fri' or day == 'Sat' then
        self.calendarLabels[index]:SetDefaultColor(colors.orange:unpack())
        print(string.format("Orange day: %s index: %d label: %s", day, index, self.calendarLabels[index]:GetText()))
      else
        self.calendarLabels[index]:SetDefaultColor(colors.black:unpack())
        print(string.format("Black  day: %s index: %d label: %s", day, index, self.calendarLabels[index]:GetText()))
      end
      self.calendarLabels[index]:SetText(day)
    else
      print(string.format("None   day: %s index: %d", day, index))
    end
  end
end

function Hud:layoutButtons()
  local padding = 3
  local totalWidth = 0

  -- Determine width of all buttons including paddding
  for index, button in ipairs(self.buttons) do
   totalWidth = totalWidth + button:GetWidth()
  end
  totalWidth = totalWidth + padding * #self.buttons

  -- Position buttons on the right edge of the HUD
  local startX = self.panel:GetWidth() - totalWidth

  for index, button in ipairs(self.buttons) do
    button:SetX(startX + ((index - 1) * (34 + padding)))
    button:CenterY()
  end
end

function Hud:receiveMessage(message, data)
  if message == 'ui_update' then
    self:update(dt)

  elseif message == 'window_reload' then
    if self.panel then
      self.panel:SetSize(System:getWidth() + 2, 24)
      self.panel:SetPos(-1, System:getHeight() - 24 + 1)
      self:layoutButtons()
      self:layoutWeather()
      self:layoutCalendar()
    end

  elseif message == 'weather_changed' then
    local forecast = data
    self:setWeatherData(forecast)

  elseif message == 'calendar_changed' then
    local week = data
    self:setCalendarData(week)
  end
end

function Hud:update(dt)
  self.incomeText:SetText(string.format('Income: $%i / month', self.player:getBaseIncome()))
  self.repBar:SetValue(self.player:getReputation())
end

function Hud:close()
  if self.panel then
    self.panel:Remove()
    self.panel = nil
  end
  Notifier:stopListeningForMessage('ui_update', self)
  Notifier:stopListeningForMessage('window_reload', self)
  Notifier:stopListeningForMessage('weather_changed', self)
  Notifier:stopListeningForMessage('calendar_changed', self)
end
