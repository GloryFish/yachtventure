require 'vector'
require 'middleclass'
require 'ui/infoframe'

ShipInfoFrame = class('ShipInfoFrame', InfoFrame)


function ShipInfoFrame:initialize(ship)
  InfoFrame.initialize(self)
  self.ship = ship
end

function ShipInfoFrame:show()
  InfoFrame.show(self)

  self.frame:SetName('Ship Details')

  local title = loveframes.Create('text', self.frame)
  title:SetText({
    {
      color = {Color(99, 150, 181):unpack()},
      font = fonts.default
    },
    self.ship:getName()
  })
  title:SetPos(5, 20)
  title:SetShadow(true)
end

