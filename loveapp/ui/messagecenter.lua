require 'middleclass'
require 'vector'

MessageCenter = class('MessageCenter')

function MessageCenter:initialize()
  Notifier:listenForMessage('ui_update', self)
  self.anchor = vector(1, System:getHeight() - 23)

  self.messagePanels = {}
end

function MessageCenter:receiveMessage(message, data)
  if message == 'ui_update' then
    self:update(data)
  end
end

function MessageCenter:layoutMessages()
  local offsetY = 0

  for index, item in ipairs(self.messagePanels) do
    offsetY = offsetY - item.panel:GetHeight() - 1
    item.position.y = self.anchor.y + offsetY
  end
end

function MessageCenter:add(message, timeout)
  local timeout = timeout or 5


  local panelObject = loveframes.Create('panel')
  panelObject:SetWidth(104)
  panelObject:MoveToTop()

  local label = loveframes.Create('text', panelObject)
  label:SetText(message)
  label:SetFont(fonts.hud)
  label:SetMaxWidth(100)
  panelObject:SetHeight(label:GetHeight() + 4)
  label:Center()

  local item = {
    panel = panelObject,
    position = vector(-panelObject:GetWidth(), self.anchor.y)
  }

  Tween.start(1, item.position, { x = self.anchor.x }, 'outBounce')

  Timer.add(timeout, function()
    Tween.start(1, item.position, {x = -panelObject:GetWidth()}, 'inBack', function()
      panelObject:Remove()
      for index, thing in ipairs(self.messagePanels) do
        if thing == item then
          table.remove(self.messagePanels, index)
        end
      end
    end)
  end)

  table.insert(self.messagePanels, item)
end

function MessageCenter:update(dt)
  self:layoutMessages()

  for index, item in ipairs(self.messagePanels) do
    item.panel:SetPos(item.position:unpack())
  end
end

function MessageCenter:close()
  for index, item in ipairs(self.messagePanels) do
    if item.panel then
      item.panel:remove()
    end
  end
  self.messagePanels = {}
  Notifier:stopListeningForMessage('ui_update', self)
end