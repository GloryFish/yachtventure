require 'vector'
require 'middleclass'
require 'ui/infoframe'

PortInfoFrame = class('PortInfoFrame', InfoFrame)


function PortInfoFrame:initialize(world, player, ship, weather)
  InfoFrame.initialize(self)
  self.world = world
  self.ship = ship
  self.player = player
  self.weather = weather
  self.repBar = nil
  self.port = nil
end

function PortInfoFrame:receiveMessage(message, data)
  InfoFrame.receiveMessage(self, message, data)

  if message == 'next_day' then
    self:layoutButtons(self.frame)
  end
end


function PortInfoFrame:showForPort(port)
  InfoFrame.show(self)

  self.port = port

  local frame = self.frame

  frame:SetName('Port Details')

  -- Port title
  local title = loveframes.Create('text', frame)
  title:SetText({
    {
      color = {Color(99, 150, 181):unpack()},
      font = fonts.default
    },
    port:getName()
  })
  title:SetPos(5, 20)
  title:SetShadow(true)

  -- Reputation bar
  self.repBar = loveframes.Create('ratingsbar', frame)
  self.repBar:SetPos(5, 40)
  self.repBar:SetStyle('small')
  self.repBar:SetValue(port:getReputationStandard())
  self.repBar:SetClickable(false)

  -- Port photo
  local photoPath = 'resources/sprites/ports/default.png'
  if love.filesystem.exists(string.format('resources/sprites/ports/%s.png', port:getID())) then
    photoPath = string.format('resources/sprites/ports/%s.png', port:getID())
  end
  local photo = loveframes.Create("image", frame)
  photo:SetImage(photoPath)
  photo:SetPos(175, 20)

  -- Port description
  local descriptionText = loveframes.Create('text', frame)
  descriptionText:SetFont(fonts.hud)
  descriptionText:SetText(port:getDescription())
  descriptionText:SetMaxWidth(125)
  descriptionText:SetPos(175, 105)

  self:layoutButtons(frame)

  Notifier:listenForMessage('ui_update', self)
  Notifier:listenForMessage('next_day', self)
end

function PortInfoFrame:layoutButtons(frame)
  -- Prepare Activity buttons
  local context = {
    player = self.player,
    ship = self.ship,
    port = self.port,
    weather = self.weather,
  }
  local shipIsHere = context.port:isInVicinity(self.ship:getPosition())

  -- Clear any existing buttons
  if self.buttons then
    for index, button in ipairs(self.buttons) do
      button[1]:Remove()
      button[2]:Remove()
    end
  end

  self.buttons = {}

  local button = nil
  local text = nil
  if not shipIsHere then
    button, text = self:createActivityButton(Activities.remote.go, context, frame)
    button:SetSize(34, 16)
    button.OnClick = function()
      Activities.remote.go.run(context)
      self:close()
    end

    table.insert(self.buttons, {button, text})
  end

  for index, activityName in ipairs(context.port.activities) do
    -- Can't throw parties on stormy days
    if activityName == 'party' and self.weather:getToday() == 'Stormy' then
      style = 'disabled'
    end

    button, text = self:createActivityButton(Activities.port[activityName], context, frame)
    button:SetSize(34, 16)
    button:SetEnabled(shipIsHere)
    button:SetClickable(shipIsHere)
    button.OnClick = function()
      Activities.port[activityName].run(context)
    end

    table.insert(self.buttons, {button, text})
  end

  for index, button in ipairs(self.buttons) do
    button[1]:SetPos(5, 54 + (index - 1) * 21)
    button[2]:SetPos(43, 55 + (index - 1) * 21)
  end
end

function PortInfoFrame:createActivityButton(activity, context, frame)
  local style = 'ok'
  if not activity.isAvailable(context) then
    style = 'disabled'
  end

  local button = loveframes.Create('imagebutton', frame)
  button:SetText(activity.name)
  button:SetImage(string.format('resources/sprites/button_%s.png', style))

  local text = loveframes.Create('text', frame)
  text:SetFont(fonts.hud)
  text:SetMaxWidth(140)
  text:SetText(activity.getDescription(context))

  return button, text
end

function PortInfoFrame:close()
  InfoFrame.close(self)
  Notifier:stopListeningForMessage('next_day', self)
end
