require 'vector'
require 'middleclass'

InfoFrame = class('InfoFrame')


function InfoFrame:initialize()
  -- UI components
  self.frame = nil
end

function InfoFrame:receiveMessage(message, data)
  if message == 'ui_update' then
    self:update(data)

  elseif message == 'window_reload' then
    self.frame:CenterWithinArea(0, 0, System:getWidth(), System:getHeight() - 20)
  end
end

function InfoFrame:close()
  if self.frame then
    self.frame:SetVisible(false)
    self.frame:Remove()
    self.frame = nil
  end

  Notifier:stopListeningForMessage('ui_update', self)
  Notifier:stopListeningForMessage('window_reload', self)
end

function InfoFrame:isOpen()
  return self.frame ~= nil and self.frame:GetVisible()
end

function InfoFrame:show()
  if self.frame then
    self.frame:Remove()
  end
  local frame = loveframes.Create('frame')

  frame:SetModal(true)
  frame:ShowCloseButton(true)
  frame:SetDraggable(false)
  frame:SetSize(300, 200)
  frame:CenterWithinArea(0, 0, System:getWidth(), System:getHeight() - 20)

  frame.OnClose = function()
    self:close()
  end

  self.frame = frame

  Notifier:listenForMessage('ui_update', self)
  Notifier:listenForMessage('window_reload', self)
end

function InfoFrame:update(dt)
end

