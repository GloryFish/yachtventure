require 'middleclass'
require 'ui/infoframe'

PartyFrame = class('PartyFrame', InfoFrame)


function PartyFrame:showForParty(partyDetails)
  InfoFrame.show(self)

  self.frame:SetName('Yacht party! Everyone is invited!')

  -- Title
  local title = loveframes.Create('text', self.frame)
  title:SetText({
    {
      color = {Color(99, 150, 181):unpack()},
      font = fonts.default
    },
    string.format('Party at %s', partyDetails.port.name),
  })
  title:SetPos(5, 20)
  title:SetShadow(true)

  -- Total
  local totalLabel = loveframes.Create('text', self.frame)
  totalLabel:SetText(string.format('Total score: %d', partyDetails.totalScore))
  totalLabel:SetPos(10, 40)
  totalLabel:SetFont(fonts.hud)

  -- Score breakdown
  local scoreLabel = loveframes.Create('text', self.frame)
  scoreLabel:SetText('Score breakdown')
  scoreLabel:SetPos(10, 50)
  scoreLabel:SetFont(fonts.hud)

  local scoreList = loveframes.Create('list', self.frame)
  scoreList:SetPos(10, 57)
  scoreList:SetSize(150, 135)
  scoreList:SetPadding(2)
  scoreList:SetSpacing(2)

  local scoreText = loveframes.Create('text')

  local scoreDetails = ''

  -- Basic
  for index, item in ipairs(partyDetails.scoring) do
    scoreDetails = string.format('%s\n%s (%d)', scoreDetails, item.description, item.score)
  end

  -- Bonuses

  scoreText:SetText(scoreDetails)
  scoreText:SetFont(fonts.hud)
  scoreList:AddItem(scoreText)
  self.scoreText = scoreText

end
