require 'vector'
require 'middleclass'
require 'ui/infoframe'

RepInfoFrame = class('RepInfoFrame', InfoFrame)


function RepInfoFrame:initialize(world, player)
  InfoFrame.initialize(self)
  self.player = player
end

function RepInfoFrame:show()
  InfoFrame.show(self)

  self.frame:SetName('Reputation Details')
end

