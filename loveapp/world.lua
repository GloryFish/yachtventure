require 'middleclass'
require 'astar'
require 'vector'
require 'port'
require 'waypoint'
require 'spatialhash'
require 'rectangle'
require 'ray'

local HC = require 'hardoncollider'

World = class('World')


function World:initialize(filename)
  self.tilemap = ATL.Loader.load(filename)
  self.tilemap.layers['collision'].visible = false
  self.tilemap.layers['game'].visible = false

  self.spatialhash = SpatialHash(100)

  self.astar = AStar(self)

  self.polygon_points = {}
  self.collider = HC(100, self.on_collision, self.collision_stop)
  self:loadWalls()
  self:loadPorts()
  self:loadGameObjects()
  self:generateWaypointGraph()
end

function World:loadWalls()
  local objects = self.tilemap.layers['collision'].objects
  self.shapes = {}

  for index, object in ipairs(objects) do
    local shape = self.collider:addPolygon(unpack(object.polygon))
    shape:move(object.x, object.y)

    self.collider:setPassive(shape)
    table.insert(self.shapes, shape)
  end
end

function World:loadPorts()
  local objects = self.tilemap.layers['ports'].objects
  self.ports = {}
  for index, object in ipairs(objects) do
    local port = Port(vector(object.x, object.y), object.name)
    table.insert(self.ports, port)
  end
end

function World:loadGameObjects()
  local objects = self.tilemap.layers['game'].objects
  for name, object in pairs(objects) do
    if object.name == 'spawn' then
      self.playerSpawn = vector(object.x, object.y) + vector(8, -8) -- Tile origin is lower left, offset to center of tile
    end
  end
end

function World:pointIsWalkable(point)
  for shape in pairs(self.collider:shapesAt(point:unpack())) do
    return false
  end
  return true
end

function World:generateWaypointGraph()
  self.waypoints = {}
  local waypoints = {}

  local start_waypoint = Waypoint(vector(0, 0))
  local spacing = 30
  local graph_width = self.tilemap.width * self.tilemap.tileWidth / spacing
  local graph_height = self.tilemap.height * self.tilemap.tileHeight / spacing

  for y = 1, graph_height do
    for x = 1, graph_width do
      local waypoint = Waypoint(vector(x * spacing - spacing, y * spacing - spacing))

      if x ~= 1 then
        local previous_waypoint = waypoints[#waypoints]
        self:connectWaypoints(previous_waypoint, waypoint)
      end

      if y ~= 1 then
        local above_waypoint = waypoints[#waypoints - graph_width + 1]
        self:connectWaypoints(above_waypoint, waypoint)
      end

      table.insert(waypoints, waypoint)
      self.spatialhash:insertObjectForRect(waypoint, waypoint:getRect())
    end
  end

  for index, waypoint in ipairs(waypoints) do
    self.waypoints[waypoint.id] = waypoint
  end

  for id, waypoint in pairs(self.waypoints) do
    -- Remove non-walkable waypoints
    if not self:pointIsWalkable(waypoint:getPosition()) then
      for conn_id, _ in pairs(waypoint.connections) do
        self.waypoints[conn_id]:removeConnectionTo(id)
      end
      self.spatialhash:removeObjectForRect(waypoint, waypoint:getRect())
      self.waypoints[id] = nil
    end
  end
end

function World:connectWaypoints(a, b)
  if a == b then
    return
  end

  a:connectTo(b.id)
  b:connectTo(a.id)
end

function World:disconnectWaypoints(a, b)
  if a == b then
    return
  end

  a:disconnectFrom(b.id)
  b:disconnectFrom(a.id)
end

function World:getPlayerSpawn()
  return self.playerSpawn or vector(0, 0)
end

function World:getBounds()
  return {
    left = 0,
    top = 0,
    right = self.tilemap.width * self.tilemap.tileWidth,
    bottom = self.tilemap.height * self.tilemap.tileHeight
  }
end

-- Finds a waypoint near 'point' within 'range', ensures that there is direct line of sight to the waypoint
function World:visibleWaypointInRange(point, range, target)
  local offset = vector(range, range)
  local rect = Rectangle(point - offset, offset * 2)

  local lookAtTarget = nil
  if target ~= nil then
    lookAtTarget = point - target
  end

  local nearest_waypoint = false
  local min_dist = math.huge
  local min_dot = math.huge

  if target ~= nil then
    for waypoint in pairs(self.spatialhash:objectsForRect(rect)) do
      local dist = point:dist(waypoint.circle.position)
      -- if this is the closest we've found and we have LOS, set this as the nearest
      local lookAt = waypoint.circle.position - point
      local dot = lookAt:dot(lookAtTarget)


      if dot < min_dot and dist < range and not self.collider:intersectsRay(point.x, point.y, lookAt.x, lookAt.y) then
        min_dot = dot
        nearest_waypoint = waypoint
      end
    end
  else
    for waypoint in pairs(self.spatialhash:objectsForRect(rect)) do
      local dist = point:dist(waypoint.circle.position)
      -- if this is the closest we've found and we have LOS, set this as the nearest
      local lookAt = waypoint.circle.position - point

      if dist < min_dist and dist < range and not self.collider:intersectsRay(point.x, point.y, lookAt.x, lookAt.y) then
        min_dist = dist
        nearest_waypoint = waypoint
      end
    end
  end

  return nearest_waypoint
end

-- Finds waypoint nearest to 'point' within 'range'
function World:nearestWaypointInRange(point, range)
  local offset = vector(range, range)
  local rect = Rectangle(point - offset, offset * 2)

  local nearest_waypoint = false
  local min_dist = math.huge

  for waypoint in pairs(self.spatialhash:objectsForRect(rect)) do
    local dist = point:dist(waypoint.circle.position)
    -- if this is the closest we've found and we have LOS, set this as the nearest
    local lookAt = waypoint.circle.position - point

    if dist < min_dist and dist < range then
      min_dist = dist
      nearest_waypoint = waypoint
    end
  end

  return nearest_waypoint
end

function World:intersectsRay(origin, delta)
  return self.collider:intersectsRay(origin.x, origin.y, delta.x, delta.y)
end


function World:pathBetween(start_point, end_point, range)
  -- get start and end waypoints
  local start_waypoint = self:visibleWaypointInRange(start_point, range, end_point)
  if not start_waypoint then
    print('no visible waypoint, getting any nearest')
    start_waypoint = self:nearestWaypointInRange(start_point, range, end_point)
  end

  local end_waypoint = self:visibleWaypointInRange(end_point, range)

  if not start_waypoint or not end_waypoint then
    print(tostring(start_waypoint))
    print(tostring(end_waypoint))
    print('sucked out')
    return nil
  end

  -- Can we just walk in a straight line?
  local lookAt = end_point - start_point
  if not self.collider:intersectsRay(start_point.x, start_point.y, lookAt.x, lookAt.y) then
    return {end_point}
  end

  -- get path between waypoints
  local astar_path = self.astar:findPath(start_waypoint, end_waypoint)
  if astar_path == nil then
    print('astar sucked out')
    return nil
  else
    print('got path')
    local path = {}
    table.insert(path, start_point)

    for index, node in ipairs(astar_path.nodes) do
      local waypoint = node.location
      table.insert(path, waypoint.circle.position)
    end

    table.insert(path, end_point)

    for index, point in ipairs(path) do
      print(point)
    end

    -- Smooth path
    if #path > 2 then
      local check_point = path[1]

      local current_index = 2
      local current_point = path[current_index]
      local next_point = path[current_index + 1]

      while next_point do
        if not self:intersectsRay(check_point, next_point - check_point) then
          current_point.deleteme = true
        else
          check_point = current_point
        end
        current_index = current_index + 1
        current_point = path[current_index]
        next_point = path[current_index + 1]
      end
    end

    for i = #path, 1, -1 do
      if path[i].deleteme then
        table.remove(path, i)
      end
    end

    return path
  end
end

function World:update(dt)
  local viewport = self.camera:worldViewport()
  self.tilemap:setDrawRange(viewport:unpack())
end

function World:draw()
  self.tilemap:draw()

  for index, port in ipairs(self.ports) do
    port:draw()
  end

  if vars.showwalls then
    colors.orange:alpha(0.5 * 255):set()
    for index, shape in ipairs(self.shapes) do
      love.graphics.push()

      shape:draw('fill')

      love.graphics.pop()
    end
  end

  -- Draw navigation graph
  if vars.showgraph then
    local viewport = self.camera:worldViewport()
    local waypoints = self.spatialhash:objectsForRect(viewport)

    for index, waypoint in pairs(waypoints) do
      local isSelected = waypoint == self.selectedWaypoint
      waypoint:draw(isSelected)

      for id, _ in pairs(waypoint.connections) do
        local secondWaypoint = self.waypoints[id]

        if secondWaypoint then
        colors.blue:set()
        love.graphics.line(waypoint.circle.position.x, waypoint.circle.position.y,
                          secondWaypoint.circle.position.x, secondWaypoint.circle.position.y)
        end
      end
    end
  end
end

-- AStar MapHandler

function World:getNode(waypoint)
  if self.waypoints[waypoint.id] == nil then
    return nil
  end

  return Node(waypoint, 10, waypoint.id)
end


function World:getAdjacentNodes(curnode, dest)
  local result = {}

  -- Process rooms according to door connections
  local currentWaypoint = self.waypoints[curnode.location.id]

  for neighbor_id, _ in pairs(currentWaypoint.connections) do
    local n = self:_handleNode(neighbor_id, curnode, dest.id)
    if n then
      table.insert(result, n)
    end
  end

  return result
end

function World:locationsAreEqual(a, b)
  return a.id == b.id
end

function World:_handleNode(id, fromnode, destid)
  -- Fetch a Node for the given location and set its parameters
  local waypoint = self.waypoints[id]
  local destWaypoint = self.waypoints[destid]

  local n = self:getNode(waypoint)

  if n ~= nil then
    local dx = math.max(waypoint.circle.position.x, destWaypoint.circle.position.x) - math.min(waypoint.circle.position.x, destWaypoint.circle.position.x)
    local dy = math.max(waypoint.circle.position.y, destWaypoint.circle.position.y) - math.min(waypoint.circle.position.y, destWaypoint.circle.position.y)
    local emCost = dx + dy

    n.mCost = n.mCost + fromnode.mCost
    n.score = n.mCost + emCost
    n.parent = fromnode

    return n
  end

  return nil
end