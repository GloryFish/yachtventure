# Yachtventure!


## Credits

### Photos

The Island - V.Fulidhoo (Maldives)
Original photo by Easa Samih
Modified, used under license (CC BY 2.0)
https://www.flickr.com/photos/eeko/5213204032

St. Maarten (18)-The charter yacht Lauren L
Original photo by Gail Frederick
Modified, used under license (CC BY 2.0)
https://www.flickr.com/photos/galfred/3157934568

Milford Marina
Original photo by Peter Harrison
Modified, used under license (CC BY 2.0)
https://www.flickr.com/photos/devcentre/2075945897

Amadjuak Bay, Nunavut
Original photo by Mike Beauregard
Modified, used under license (CC BY 2.0)
https://www.flickr.com/photos/31856336


### Music

The Drunken Monkey
by christofori
http://modarchive.org/index.php?request=view_by_moduleid&query=41544
Public Domain

