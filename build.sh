# Clean build directory
rm -rf build/*

# Export release info
git rev-parse --short HEAD > loveapp/release.txt

# Build .love
cd loveapp
zip -r ../build/yachtventure.love *

cd ../build

# build Windows
cp -R ../love-windows ./yachtventure-win
cat ./yachtventure-win/love.exe ./yachtventure.love > ./yachtventure-win/yachtventure.exe
rm ./yachtventure-win/love.exe

# Build Mac

cp -R /Applications/love.app ../build/Yachtventure.app
cp ./yachtventure.love ./Yachtventure.app/Contents/Resources/
/usr/libexec/PlistBuddy -c "Set :CFBundleIdentifier org.gloryfish.yachtventure" ./Yachtventure.app/Contents/Info.plist
/usr/libexec/PlistBuddy -c "Set :CFBundleName Yachtventure!" ./Yachtventure.app/Contents/Info.plist
/usr/libexec/PlistBuddy -c "Set :CFBundleSignature org.gloryfish.yachtventure" ./Yachtventure.app/Contents/Info.plist
/usr/libexec/PlistBuddy -c "Delete :UTExportedTypeDeclarations" ./Yachtventure.app/Contents/Info.plist
/usr/libexec/PlistBuddy -c "Delete :CFBundleDocumentTypes" ./Yachtventure.app/Contents/Info.plist

cp ../icon.icns ./Yachtventure/Contents/Resources/Love.icns

# Move build files into Dropbox
rm -rf ~/Dropbox/Games/Yachtventure/latest_build/*
cp -a ./* ~/Dropbox/Games/Yachtventure/latest_build/